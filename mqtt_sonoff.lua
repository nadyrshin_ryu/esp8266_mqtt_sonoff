WIFI_SSID = "ZyXEL-R"
WIFI_PASS = "123hlk057"
MQTT_BrokerIP = "192.168.1.35"
MQTT_BrokerPort = 1883
MQTT_ClientID = "esp-011"
MQTT_Client_user = "user"
MQTT_Client_password = "password"
MQTT_RelayTopicPath = "/ESP/Relays/"
MQTT_Relay_ID = "001"
RELAY_PIN = 6
LED_PIN = 7
BUTTON_PIN = 3
--DHT_PIN = 7

gpio.mode(RELAY_PIN, gpio.OUTPUT) 
gpio.mode(LED_PIN, gpio.OUTPUT)
gpio.mode(BUTTON_PIN, gpio.INPUT)
gpio.write(RELAY_PIN, gpio.LOW)
gpio.write(LED_PIN, gpio.HIGH)
wifi.setmode(wifi.STATION)
wifi.sta.config(WIFI_SSID, WIFI_PASS)
wifi.sta.connect()

local wifi_status_old = 0
local RelayState_old = 0
local ButtonState_old = 0

local function switchRelay(id, state)
    if (id == MQTT_Relay_ID) then
        if (state == 0) or (state == "0") or (string.lower(state) == "off") then
            if (RelayState_old == 1) then
                RelayState_old = 0
                gpio.write(RELAY_PIN, gpio.LOW)
                gpio.write(LED_PIN, gpio.HIGH)
                --print("RELAY OFF")
                if (m ~= nil) then
                    m:publish(MQTT_RelayTopicPath..MQTT_Relay_ID, "0", 0, 1, function(conn) 
                        --print("Relay state sent") 
                    end)
                end
            end
        else
            if (RelayState_old == 0) then
                RelayState_old = 1
                gpio.write(RELAY_PIN, gpio.HIGH)
                gpio.write(LED_PIN, gpio.LOW)
                --print("RELAY ON")
                if (m ~= nil) then
                    m:publish(MQTT_RelayTopicPath..MQTT_Relay_ID, "1", 0, 1, function(conn) 
                        --print("Relay state sent") 
                    end)
                end
            end
        end
    end
end

-- Сканирование состояния кнопки из таймера 2
tmr.alarm(2, 100, tmr.ALARM_AUTO, function()
    local ButtonState = gpio.read(BUTTON_PIN)

    -- Нажатие кнопки (задний фронт на gpio0)
    if (ButtonState == 0) and (ButtonState_old == 1) then
        --print("Button pressed")
        if (RelayState_old == 0) then
            switchRelay(MQTT_Relay_ID, 1)
        else
            switchRelay(MQTT_Relay_ID, 0)
        end
    end
    ButtonState_old = ButtonState
end)

tmr.alarm(0, 5000, 1, function()
    print("tmr0 "..wifi_status_old.." "..wifi.sta.status())

    if wifi.sta.status() == 5 then -- подключение есть
        if wifi_status_old ~= 5 then -- Произошло подключение к Wifi, IP получен
            print(wifi.sta.getip())

            m = mqtt.Client(MQTT_ClientID, 120, MQTT_Client_user, MQTT_Client_password)

            -- Определяем обработчики событий от клиента MQTT
            m:on("connect", function(client) print ("connected") end)
            m:on("offline", function(client) 
                tmr.stop(1)
                print ("offline") 
            end)
            m:on("message", function(client, topic, data) 
                --print(topic .. ":" ) 
                if data ~= nil then
                    --print(data)
                end

                local _, RelayPos = string.find(topic, MQTT_RelayTopicPath.."(%w)")
                local Relay = string.sub(topic, RelayPos)
                --print(Relay)
                if data ~= nil then
                    switchRelay(Relay, data)
                end
            end)

            m:connect(MQTT_BrokerIP, MQTT_BrokerPort, 0, 1, function(conn) 
                print("connected")
            
                -- Подписываемся на топики если нужно
                m:subscribe(MQTT_RelayTopicPath.."#",0, function(conn) 
					print("Subscribed!")
				end)
                    
                tmr.alarm(1, 3000, 1, function()
                    -- Делаем измерения, публикуем их на брокере
                    --local status,temp,humi,temp_decimal,humi_decimal = dht.read(DHT_PIN)

                    --if (status == dht.OK) then
                    --    print("Temp: "..temp.."."..temp_decimal.." C")
                    --    print("Hum: "..humi.."."..humi_decimal.." %")
                    --    m:publish("/ESP/DHT/TEMP", temp.."."..temp_decimal, 0, 0, function(conn) print("sent") end)
                    --    m:publish("/ESP/DHT/HUM", humi.."."..humi_decimal, 0, 0, function(conn) print("sent") end)
                    --end
                end)
            end)
        else
            -- подключение есть и не разрывалось, ничего не делаем
        end
    else
        print("Reconnect "..wifi_status_old.." "..wifi.sta.status())
        tmr.stop(1)
        wifi.sta.connect()
    end

    -- Запоминаем состояние подключения к Wifi для следующего такта таймера
    wifi_status_old = wifi.sta.status()
end)
